FROM python:3.7

RUN mkdir -p /opt/app-root 
ENV APP_ROOT=/opt/app-root

ADD https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz \
/opt/oc/release.tar.gz

RUN apt-get install -y ca-certificates
RUN tar --strip-components=1 -xzvf /opt/oc/release.tar.gz -C /opt/oc/ && \ 
    mv /opt/oc/oc /usr/local/bin && \
    rm -rf /opt/oc
RUN apt-get update && apt-get install -y vim && apt-get install -y net-tools
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

RUN pip install numpy
RUN pip install scipy
RUN pip install matplotlib
RUN pip install networkx
RUN pip install selenium
RUN pip install pandas 
RUN pip install Django
RUN pip install lxml
RUN pip install requests

COPY . .

COPY . $APP_ROOT